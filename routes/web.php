<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::middleware('can:have,App\Models\Cart')->group(function () {
        Route::get('/cart', 'CartController@show')->name('cart.show');
        Route::post('/cart/add', 'CartController@addProduct')->name('cart.addProduct');
        Route::post('/cart/delete', 'CartController@deleteProduct')->name('cart.deleteProduct');
        Route::post('/cart/update', 'CartController@updateQuantity')->name('cart.updateQuantity');
        Route::get('/cart/clear', 'CartController@clear')->name('cart.clear');
        Route::post('/cart/checkout', 'CartController@checkout')->name('cart.checkout');

        Route::get('/orders', 'OrdersController@index')->name('orders.index');
    });

    Route::middleware('can:manage,App\Models\Category')->group(function () {
        Route::get('/categories/create', 'CategoriesController@create')->name('categories.create');
        Route::post('/categories', 'CategoriesController@store')->name('categories.store');
        Route::get('/categories', 'CategoriesController@index')->name('categories.index');
        Route::get('/categories/{category}/edit', 'CategoriesController@edit')->name('categories.edit');
        Route::put('/categories/{category}', 'CategoriesController@update')->name('categories.update');
        Route::get('/categories/{category}/delete', 'CategoriesController@delete')->name('categories.delete');
    });

    Route::middleware('can:manage,App\Models\Manufacturer')->group(function () {
        Route::get('/manufacturers/create', 'ManufacturersController@create')->name('manufacturers.create');
        Route::post('/manufacturers', 'ManufacturersController@store')->name('manufacturers.store');
        Route::get('/manufacturers', 'ManufacturersController@index')->name('manufacturers.index');
        Route::get('/manufacturers/{manufacturer}/edit', 'ManufacturersController@edit')->name('manufacturers.edit');
        Route::put('/manufacturers/{manufacturer}', 'ManufacturersController@update')->name('manufacturers.update');
        Route::get('/manufacturers/{manufacturer}/delete', 'ManufacturersController@delete')->name('manufacturers.delete');
    });

    Route::middleware('can:manage,App\Models\Supplier')->group(function () {
        Route::get('/suppliers/create', 'SuppliersController@create')->name('suppliers.create');
        Route::post('/suppliers', 'SuppliersController@store')->name('suppliers.store');
        Route::get('/suppliers', 'SuppliersController@index')->name('suppliers.index');
        Route::get('/suppliers/{supplier}/delete', 'SuppliersController@delete')->name('suppliers.delete');
    });

    Route::middleware('can:manage,App\Models\Contract')->group(function () {
        Route::get('/suppliers/{supplier}/contracts/create', 'ContractsController@create')->name('contracts.create');
        Route::post('/suppliers/{supplier}/contracts', 'ContractsController@store')->name('contracts.store');
        Route::get('/suppliers/{supplier}/contracts', 'ContractsController@index')->name('contracts.index');
        Route::get('/suppliers/{supplier}/contracts/{contract}/delete', 'ContractsController@delete')->name('contracts.delete');
    });

    Route::middleware('can:manage,App\Models\Product')->group(function() {
        Route::get('/products/create', 'ProductsController@create')->name('products.create');
        Route::post('/products', 'ProductsController@store')->name('products.store');
        Route::get('/products', 'ProductsController@index')->name('products.index');
        Route::get('/products/{product}/edit', 'ProductsController@edit')->name('products.edit');
        Route::put('/products/{product}', 'ProductsController@update')->name('products.update');
        Route::patch('/products/{product}/update-price', 'ProductsController@updatePrice')->name('products.update-price');
        Route::get('/products/{product}/delete', 'ProductsController@delete')->name('products.delete');
    });

    Route::middleware('can:manage,App\Models\Delivery')->group(function() {
        Route::get('/pending-deliveries', 'PendingDeliveriesController@index')->name('pending-deliveries.index');
        Route::post('/pending-deliveries/{delivery}/ship', 'PendingDeliveriesController@ship')->name('pending-deliveries.ship');
    });

    Route::post('/products/{product}/favorites', 'FavoritesController@store')->name('favorites.store');
    Route::delete('/products/{product}/favorites', 'FavoritesController@destroy')->name('favorites.destroy');

    Route::get('/account', 'AccountController@show')->name('account.show');
    Route::patch('/account', 'AccountController@update')->name('account.update');

    Route::post('/addresses', 'AddressesController@store')->name('addresses.store');
    Route::delete('/addresses/{address}', 'AddressesController@destroy')->name('addresses.destroy');

    Route::put('/primary-address', 'PrimaryAddressController@update')->name('primary-address.update');
});

Route::get('/', 'FrontPageController@index')->name('frontPage.index');
Route::get('/products/filtered', 'ProductsController@filtered')->name('products.filtered');
