# The "Dirty Bits" Team
 - Benjamin Ramić
 - Emina Pezo
 - Emir Bećirović
 - Majra Dlakić
 - Omar Sokolović
 - Sanela Mešić
 - Sanil Musić

# How to run?
 1. Clone the repository
 2. Navigate to the project root
 3. Install PHP dependencies: `composer install`
 4. Install Node dependencies: `npm install` or `yarn install`
 5. Run the application: `php artisan serve`
 6. The application is now available at [http://localhost:8000](http://localhost:8000)
 