@if (session()->has('alert'))
    <script>swal(@json(session()->pull('alert')))</script>
@endif