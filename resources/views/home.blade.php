@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 bordered shadowed">
            <h3>Dashboard</h3>
            <hr>

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <div class="row">
                <div class="col-sm-6">
                    <h3 class="text-center">Unavailable Items</h3>
                    <table class="table table-hover table-dark">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Quantity</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($unavailableItems as $item)
                                <tr>
                                    <th scope="row">{{ $item->name }}</th>
                                    <td>{{ $item->available_quantity }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-6">
                    <h3 class="text-center">Soon To Be Unavailable</h3>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Quantity</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($soonToBeUnavailable as $item)
                                <tr>
                                    <th scope="row">{{ $item->name }}</th>
                                    <td>{{ $item->availableQuantity }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
