@extends('layouts.app')

@section('content')
    <div class="container pt-3">
        <h4 class="border-bottom mb-2">Previous Orders</h4>

        @if ($orders->isEmpty())
            <div class="alert alert-info">
                You have not made any orders so far. Below is a list of the currently most popular products which are available immediately.
            </div>

            <h3 class="mt-4 text-center">Most Popular</h3>

            @include('common.product-list', ['products' => $popular_products])
        @else
            <div class="list-group">
                @foreach ($orders as $order)
                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-3">
                                @foreach ($order->products as $product)
                                    <strong>{{ $product->pivot->quantity }}</strong>
                                    <span class="text-muted">&times;</span>
                                    {{ $product->name }}
                                    <br>
                                @endforeach

                                <hr class="mt-1 mb-1">

                                Total: <strong>{{ format_currency($order->total) }}</strong>
                            </div>

                            <div class="col-md-3">
                                <strong>{{ $order->delivery->name }}</strong>
                                <br>
                                <span class="text-muted">
                                    {{ $order->delivery->street }}
                                    <br>
                                    {{ $order->delivery->postal_code }} {{ $order->delivery->city }}
                                </span>
                            </div>

                            <div class="col">
                                @if ($order->delivery->isShipped())
                                    <button class="btn btn-primary" disabled>You're order is on its way!</button>
                                    <p>We've packaged and delivered your order on <strong>{{ $order->delivery->shipped_at->toReadableDate() }}.</strong> Please allow it a couple of days before it's delivered by the post office.</p>
                                @else
                                    @if ($order->delivery->getPriority()->equals(\App\Enums\Priority::high()))
                                        <button class="btn btn-danger" disabled>Waiting to be packaged and delivered</button>
                                        <p>It usually takes us less than <strong>3 business days</strong> to package and deliver your order. It appears that this order is taking longer than usual. We apologize for any inconvenice.</p>
                                    @elseif ($order->delivery->getPriority()->equals(\App\Enums\Priority::medium()))
                                        <button class="btn btn-warning" disabled>Waiting to be packaged and delivered</button>
                                        <p>It usually takes us less than <strong>3 business days</strong> to package and deliver your order. We've received more orders than usual in the last couple of days. Thank you for your patience.</p>
                                    @else
                                        <button class="btn btn-light" disabled>Waiting to be packaged and delivered</button>
                                        <p>It usually takes us less than <strong>3 business days</strong> to package and deliver your order.
                                    @endif
                                @endif
                            </div>

                            <div class="col flex-grow-0 text-right">
                                <button class="btn btn-outline-primary" title="Feel free to click this button if you have any questions. We'll contact you as soon as possible." data-toggle="tooltip">
                                    <span class="fas fa-life-ring fa-fw"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
@endsection
