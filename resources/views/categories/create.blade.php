@extends('layouts.container')

@section('container')
    <h3 class="mb-3">Create a Category</h3>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">Administration</li>
        <li class="breadcrumb-item"><a href="{{ route('categories.index') }}">Categories</a></li>
        <li class="breadcrumb-item active">Create a Category</li>
    </ol>

    <form method="POST" action="{{ route('categories.store') }}">
        @csrf

        <div class="form-group row">
            <label for="name" class="col-md-2 col-form-label">Name</label>

            <div class="col-md-10">
                <input
                    id="name"
                    type="text"
                    class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                    name="name"
                    value="{{ old('name') }}"
                    placeholder="Women's Sneakers"
                    required
                    autofocus
                >

                @if ($errors->has('name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="description" class="col-md-2 col-form-label">Description</label>

            <div class="col-md-10">
                <textarea
                    id="description"
                    class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                    name="description"
                >{{ old('description') }}</textarea>

                @if ($errors->has('description'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="parent_category_id" class="col-md-2 col-form-label">Parent Category</label>

            <div class="col-md-10">
                <select-category name="parent_category_id" :categories="{{ $categories }}"></select-category>

                @if ($errors->has('parent_category_id'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('parent_category_id') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="text-right">
            <button type="submit" class="btn btn-primary">
                <span class="fas fa-plus-circle fa-fw"></span> Create Category
            </button>
        </div>
    </form>
@endsection
