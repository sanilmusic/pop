@extends('layouts.container')

@section('container')
    <div class="row mb-3 align-items-center">
        <div class="col">
            <h3 class="mb-0">Categories</h3>
        </div>

        <div class="col flex-grow-0">
            <a href="{{ route('categories.create') }}" class="btn btn-primary">
                <span class="fas fa-plus-circle fa-fw"></span> Create a Category
            </a>
        </div>
    </div>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">Administration</li>
        <li class="breadcrumb-item active">Categories</li>
    </ol>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($categories as $category)
                <tr>
                    <td>
                        {{ $category->name }}
                        <small>{{ $category->getHierarchyPath() }}</small>
                    </td>
                    <td class="text-right text-nowrap">
                        <a href="{{ route('categories.edit', $category) }}" class="btn btn-outline-primary">
                            <span class="fas fa-edit fa-fw"></span>
                        </a>

                        <link-delete href="{{ route('categories.delete', $category) }}"></link-delete>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="row justify-content-center">
        <div class="col flex-grow-0">
            {{ $categories->links() }}
        </div>
    </div>
@endsection
