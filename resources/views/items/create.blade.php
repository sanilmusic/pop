@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-10 offset-sm-1 bordered shadowed">
                <h3>Add item</h3>
                <hr />
                <form method="POST" action="{{ route('items.store') }}">
                    @csrf

                    <div class="form-group row">
                        <label for="name" class="col-md-2 col-form-label">Item definiton</label>

                        <div class="col-md-10">
                            <select class="custom-select mr-sm-2 form-control {{ $errors->has('product_id') ? ' is-invalid' : '' }}" name="product_id">
                                <option selected disabled> Select </option>
                                @foreach ($products as $product)
                                    <option value="{{ $product->id }}"> {{ $product->name }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('product_id'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('product_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="quantity" class="col-md-2 col-form-label">Quantity</label>

                        <div class="col-md-10">
                            <input id="quantity" type="number" min="1" class="form-control{{ $errors->has('quantity') ? ' is-invalid' : '' }}"
                                  name="quantity" value="{{ old('quantity', 1) }}" required>

                            @if ($errors->has('quantity'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row mb-0 text-right">
                        <div class="col-md-10 offset-md-2">
                            <button type="submit" class="btn btn-primary">
                                Add new item
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
