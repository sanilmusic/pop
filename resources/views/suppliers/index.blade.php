@extends('layouts.container')

@section('container')
    <div class="row mb-3 align-items-center">
        <div class="col">
            <h3 class="mb-0">Suppliers</h3>
        </div>

        <div class="col flex-grow-0">
            <a href="{{ route('suppliers.create') }}" class="btn btn-primary">
                <span class="fas fa-plus-circle fa-fw"></span> Create a Supplier
            </a>
        </div>
    </div>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">Administration</li>
        <li class="breadcrumb-item active">Suppliers</li>
    </ol>

    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            @foreach($suppliers as $supplier)
                <tr>
                    <td>{{ $supplier->name }}</td>
                    <td>{{ $supplier->description }}</td>
                    <td class="text-right">
                        <link-delete href="{{ route('suppliers.delete', $supplier) }}"></link-delete>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="row justify-content-center">
        <div class="col flex-grow-0">
            {{ $suppliers->links() }}
        </div>
    </div>
@endsection
