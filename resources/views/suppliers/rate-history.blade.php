@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row bordered shadowed">
            <!-- list of suppliers -->
            <div class="col-sm-12">
                <h3 >History of ratings</h3>
                <form method="GET" action="{{ route('suppliers.rate-history')}}">
                <div class="form-group row">
                    <label for="selectSuppliersMarkSupplier" class="col-sm-2 col-form-label">Supplier:</label>
                    <div class="col-sm-9">
                        <select class="custom-select mr-sm-2" id="selectSuppliersMarkSupplier" name="supplier_id">
                            <option selected disabled> All </option>
                            @foreach($suppliers as $supplier)
                                <option value="{{$supplier->id}}"> {{$supplier->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-1">
                        <button type="submit" class="btn btn-primary">Pick</button>
                    </div>
                </div>
                </form>
                <hr />
                <table class="table table-striped text-right">
                    <thead>
                    <tr>
                        <th scope="col">Supplier</th>
                        <th scope="col">Price</th>
                        <th scope="col">Shipment speed</th>
                        <th scope="col">Product quality</th>
                        <th scope="col">Satisfaction</th>
                        <th scope="col">Rating was</th>
                        <th scope="col">Rating date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($showsuppliers as $supplier)
                        @foreach($supplier->supplierRate as $rate)
                            <tr>
                                <td scope="row">{{ $supplier->name }}</td>
                                <td>{{$rate->price}}</td>
                                <td>{{$rate->shipment_speed}}</td>
                                <td>{{$rate->product_quality}}</td>
                                <td>{{$rate->satisfaction}}</td>
                                <td>{{$rate->created_at->diffForHumans() }}</td>
                                <td>{{Carbon\Carbon::parse($supplier->supplierRate[0]->created_at)->format('d.m.Y') }}</td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection