@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row bordered shadowed">
            <!-- list of suppliers -->
            <div class="col-sm-6">
                <h3 >Current ratings</h3>
                <hr />
                <table class="table table-striped text-right">
                    <thead>
                    <tr>
                        <th scope="col">Supplier</th>
                        <th scope="col">Price</th>
                        <th scope="col">Shipment speed</th>
                        <th scope="col">Product quality</th>
                        <th scope="col">Satisfaction</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($ratings as $rating)

                        <tr>
                            <td scope="row">{{ $rating->supplier->name }}</td>
                            <td>{{$rating->price}}</td>
                            <td>{{$rating->shipment_speed}}</td>
                            <td>{{$rating->product_quality}}</td>
                            <td>{{$rating->satisfaction}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- form for ratings -->
            <div class="col-sm-6">
                <form method="POST" action="{{ route('suppliersRate.create') }}">
                    @csrf
                    <h3>Rate Supplier</h3>
                    <hr />
                    <div class="form-group row">
                        <label for="selectSuppliersMarkSupplier" class="col-sm-2 col-form-label">Supplier:</label>
                        <div class="col-sm-10">
                            <select class="custom-select mr-sm-2" id="selectSuppliersMarkSupplier" name="supplier_id">
                                @foreach($suppliers as $supplier)
                                 <option value="{{$supplier->id}}"> {{$supplier->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <!--Ispod imamo ocjene za odabranog dobavljaca-->
                    <h4>Ratings</h4>
                    <hr />
                    <div class="form-group row">
                        <label for="inputPriceMarkSupplier" class="col-sm-4 col-form-label">Price</label>
                        <div class="col-sm-8">
                            <input type="number" min="1" max="5" class="form-control" id="inputPriceMarkSupplier" name="price"
                                   placeholder="Enter price criterion rating" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputSpeedMarkSupplier" class="col-sm-4 col-form-label">Shipment speed</label>
                        <div class="col-sm-8">
                            <input type="number" min="1" max="5" class="form-control" id="inputSpeedMarkSupplier" name="shipment_speed"
                                   placeholder="Enter shipment speed criterion rating" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputQualityMarkSupplier" class="col-sm-4 col-form-label">Product quality</label>
                        <div class="col-sm-8">
                            <input type="number" min="1" max="5" class="form-control" id="inputQualityMarkSupplier" name="product_quality"
                                   placeholder="Enter product quality criterion rating" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputSatisfactionMarkSupplier" class="col-sm-4 col-form-label">Satisfaction</label>
                        <div class="col-sm-8">
                            <input type="number" min="1" max="5" class="form-control" id="inputSatisfactionMarkSupplier" name="satisfaction"
                                   placeholder="Enter satisfaction criterion rating" />
                        </div>
                    </div>
                    <div class="form-group row text-right">
                        <div class="col-sm-10 offset-md-2">
                            <button type="submit" class="btn btn-primary">Rate this supplier</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection