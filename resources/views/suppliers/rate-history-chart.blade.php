@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row bordered shadowed">
            <!-- list of suppliers -->
            <div class="col-sm-12">
                <h3 >History of ratings - charts</h3>
                <form method="GET" action="{{ route('suppliers.rate-chart')}}">
                <div class="form-group row">
                    <label for="selectSuppliersMarkSupplier" class="col-sm-2 col-form-label">Supplier:</label>
                    <div class="col-sm-9">
                        <select class="custom-select mr-sm-2" id="selectSuppliersMarkSupplier" name="supplier_id">
                            <option selected disabled> Select </option>
                            @foreach($suppliers as $supplier)
                                <option value="{{$supplier->id}}"> {{$supplier->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-1">
                        <button type="submit" class="btn btn-primary">Pick</button>
                    </div>
                </div>
                </form>
                <hr />
                @if($selectedSupplier)
                    <history-chart
                            :supplier-rates="{{ $arrayOfGrades }}"
                            supplier-name="{{ $selectedSupplier->name }}">
                    </history-chart>
                @else
                    <h1 class="text-center">First select the supplier.</h1>
                @endif
            </div>
        </div>
    </div>
@endsection