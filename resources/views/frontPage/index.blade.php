@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        @include('frontPage.carousel')

        <div class="container">
            <h3 class="mt-4 text-center">Most Popular</h3>

            @include('common.product-list', ['products' => $popular_products])
        </div>
    </div>
@endsection
