<div class="row dark-bg justify-content-center">
    <div class="col-sm-6">
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel" data-slide-to="0" class="active"></li>
                <li data-target="#carousel" data-slide-to="1"></li>
                <li data-target="#carousel" data-slide-to="2"></li>
            </ol>

            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img
                        class="carousel-img"
                        src="https://news.nike.com/images/swoosh-default.jpg"
                        alt="Image Unavailable"
                    >

                    <div class="carousel-caption d-none d-md-block">
                        <h3>Nike</h3>
                        <p>The Latest Models!</p>
                    </div>
                </div>

                <div class="carousel-item">
                    <img
                        class="carousel-img"
                        src="https://smhttp-ssl-33667.nexcesscdn.net/manual/wp-content/uploads/2016/07/adidas-superstar-black-white-gold-1170x638.jpg"
                        alt="Image Unavailable"
                    >
                </div>

                <div class="carousel-item">
                    <img
                        class="carousel-img"
                        src="https://rubicon.run/wp-content/uploads/2017/02/puma-running-banner.jpg"
                        alt="Image Unavailable"
                    >
                </div>
            </div>

            <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>