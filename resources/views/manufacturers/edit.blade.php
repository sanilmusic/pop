@extends('layouts.container')

@section('container')
    <h3 class="mb-3">Edit a Manufacturer</h3>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">Administration</li>
        <li class="breadcrumb-item"><a href="{{ route('manufacturers.index') }}">Manufacturers</a></li>
        <li class="breadcrumb-item active">Edit a Manufacturer</li>
    </ol>

    <form method="POST" action="{{ route('manufacturers.update', $manufacturer) }}">
        @csrf

        <input type="hidden" name="_method" value="put">

        <div class="form-group row">
            <label for="name" class="col-md-2 col-form-label">Name</label>

            <div class="col-md-10">
                <input
                    id="name"
                    type="text"
                    class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                    name="name"
                    value="{{ old('name', $manufacturer->name) }}"
                    placeholder="Nike"
                    required
                    autofocus
                >

                @if ($errors->has('name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="description" class="col-md-2 col-form-label">Description</label>

            <div class="col-md-10">
                <textarea
                    id="description"
                    class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                    name="description"
                >{{ old('description', $manufacturer->description) }}</textarea>

                @if ($errors->has('description'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="text-right">
            <button type="submit" class="btn btn-primary">
                <span class="fas fa-plus-circle fa-fw"></span> Save Manufacturer
            </button>
        </div>
    </form>
@endsection
