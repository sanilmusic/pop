@extends('layouts.container')

@section('container')
    <div class="row mb-3 align-items-center">
        <div class="col">
            <h3 class="mb-0">Manufacturers</h3>
        </div>

        <div class="col flex-grow-0">
            <a href="{{ route('manufacturers.create') }}" class="btn btn-primary">
                <span class="fas fa-plus-circle fa-fw"></span> Create a Manufacturer
            </a>
        </div>
    </div>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">Administration</li>
        <li class="breadcrumb-item active">Manufacturers</li>
    </ol>

    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($manufacturers as $manufacturer)
                <tr>
                    <td>{{ $manufacturer->name }}</td>
                    <td>{{ $manufacturer->description }}</td>
                    <td class="text-right text-nowrap">
                        <a href="{{ route('manufacturers.edit', $manufacturer) }}" class="btn btn-outline-primary">
                            <span class="fas fa-edit fa-fw"></span>
                        </a>
                        
                        <link-delete href="{{ route('manufacturers.delete', $manufacturer) }}"></link-delete>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="row justify-content-center">
        <div class="col flex-grow-0">
            {{ $manufacturers->links() }}
        </div>
    </div>
@endsection
