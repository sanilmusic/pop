@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row bordered shadowed">
            <div class="col-sm-12">
                @if (Session::has('addedToCartMessage'))
                    <div class="alert alert-info">{{ Session::get('addedToCartMessage') }}</div>
                @endif
            </div>
            @if(count($items) > 0)
            @foreach($items as $item)
                    <div class="col-sm-3 text-center d-flex flex-column">
                        <a href="#" class="thumbnail flex-grow-1">
                            <img src="{{$item->img_url}}"
                                 alt="Image not available" style="max-width: 200px; max-height: 200px;">
                        </a>
                        <div class="caption text-center">
                            <h5>{{ $item->name }}</h5>
                            <p>{{ format_currency($item->price) }}</p>
                            <p>

                                @guest
                                    <a href="{{ route('login') }}" class="btn btn-primary">
                                        @csrf
                                        Buy
                                    </a>
                                @else
                                    <form method="POST" action="{{ route('userCart.add') }}">
                                        <div class="input-group mb-3">
                                            <input type="number" min="1" max="{{ $item->availableQuantity }}" value="1" name="quantity" class="form-control">
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-primary" role="button" data-toggle="modal" data-target="#purchaseModal">
                                                    <input type="hidden" name="productId" value="{{$item->id}}">
                                                    @csrf
                                                    Buy
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                @endguest
                            </p>
                        </div>
                    </div>
            @endforeach
            @else
                <div class="col-sm-12 text-center">
                    <h1 > No Items. </h1>
                </div>
            @endif
        </div>
    </div>
@endsection
