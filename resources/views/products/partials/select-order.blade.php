<select id="order" name="order" class="custom-select" placeholder="Order by...">
    <option
        value="price_lh"
        {{ request('order', $default) === $default ? 'selected' : '' }}
    >Price (Low to High)</option>

    <option
        value="price_hl"
        {{ request('order', $default) === 'price_hl' ? ' selected' : '' }}
    >Price (High to Low)</option>

    <option
        value="latest"
        {{ request('order', $default) === 'latest' ? ' selected' : '' }}
    >What's New</option>

    <option
        value="favorites_first"
        {{ request('order', $default) === 'favorites_first' ? ' selected' : '' }}
    >Favorites First</option>

    <option
        value="favorites_last"
        {{ request('order', $default) === 'favorites_last' ? ' selected' : '' }}
    >Favorites Last</option>

    <option
        value="popularity"
        {{ request('order', $default) === 'popularity' ? ' selected' : '' }}
    >Popularity</option>

    <option
        value="alpha"
        {{ request('order', $default) === 'alpha' ? ' selected' : '' }}
    >Alphabetically</option>
</select>