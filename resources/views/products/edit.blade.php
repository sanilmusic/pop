@extends('layouts.container')

@section('container')
    <h3 class="mb-3">Edit a Product</h3>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">Administration</li>
        <li class="breadcrumb-item"><a href="{{ route('products.index') }}">Products</a></li>
        <li class="breadcrumb-item active">Edit a Product</li>
    </ol>

    <form
        method="POST"
        action="{{ route('products.update', $product) }}"
        enctype="multipart/form-data"
    >
        @csrf

        <input type="hidden" name="_method" value="put">

        <div class="form-group row">
            <label for="name" class="col-md-2 col-form-label">Name</label>

            <div class="col-md-10">
                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $product->name) }}" placeholder="Sneakers" required autofocus>

                @if ($errors->has('name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="description" class="col-md-2 col-form-label">Description</label>

            <div class="col-md-10">
                <textarea
                    id="description"
                    class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                    name="description"
                    required
                >{{ old('description', $product->description) }}</textarea>

                @if ($errors->has('description'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="price" class="col-md-2 col-form-label">Price</label>

            <div class="col-md-10">
                <div class="input-group">
                    <input
                        id="price"
                        type="number"
                        step="0.01"
                        class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}"
                        name="price"
                        value="{{ old('price', $product->getPriceInDollars()) }}"
                        placeholder="99.99"
                        required
                    >

                    <div class="input-group-append">
                        <span class="input-group-text">KM</span>
                    </div>
                </div>

                @if ($errors->has('price'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('price') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="category_id" class="col-md-2 col-form-label">Category</label>

            <div class="col-md-10">
                <select-category
                    name="category_id"
                    :categories="{{ $categories }}"
                    :value="@json(old('category_id', $product->category_id))"
                ></select-category>

                @if ($errors->has('category_id'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('category_id') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="manufacturer_id" class="col-md-2 col-form-label">Manufacturer</label>

            <div class="col-md-10">
                <select-manufacturer
                    name="manufacturer_id"
                    :manufacturers="{{ $manufacturers }}"
                    :value="{{ $product->manufacturer_id }}"
                ></select-manufacturer>

                @if ($errors->has('manufacturer_id'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('manufacturer_id') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="text-right">
            <button type="submit" class="btn btn-primary">
                <span class="fas fa-plus-circle fa-fw"></span> Save Product
            </button>
        </div>
    </form>
@endsection
