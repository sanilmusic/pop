@extends('layouts.app')

@section('content')
    <div class="container mt-3">
        <h3 class="text-center">Products</h3>

        <div class="row">
            <div class="col-md-3">
                <div class="filter-box">
                    <form method="get" action="{{ route('products.filtered') }}">
                        <div class="form-group">
                            <label for="order">Order</label>
                            @include('products.partials.select-order', [
                                'default' => 'price_lh'
                            ])
                        </div>

                        <hr>

                        <div class="form-group">
                            <label for="manufacturer">Brand</label>

                            <select-manufacturer
                                name="manufacturer"
                                :manufacturers="{{ $manufacturers }}"
                                :value="@json(request('manufacturer') ? (int) request('manufacturer') : null)"
                            />
                        </div>

                        <div class="form-group">
                            <label for="category">Category</label>

                            <select-category
                                name="category"
                                :categories="{{ $categories }}"
                                :value="@json(request('category') ? (int) request('category') : null)"
                            ></select-category>
                        </div>

                        <div class="form-group">
                            <label for="budget">Budget</label>

                            <div class="form-row mb-2">
                                <div class="col">
                                    <strong>{{ format_currency($min_price) }}</strong>
                                </div>

                                <div class="col text-right">
                                    <strong>{{ format_currency($max_price) }}</strong>
                                </div>
                            </div>

                            <range-price
                                name="budget"
                                :min="{{ (double) $min_price }}"
                                :max="{{ (double) $max_price }}"
                                :initial-value="{{ (double) request('budget', $max_price) }}"
                            />
                        </div>

                        <div class="form-group">
                            <div class="form-check">
                                <input
                                    id="hide-unavailable"
                                    name="hide_unavailable"
                                    type="checkbox"
                                    class="form-check-input"
                                    {{ request()->has('hide_unavailable') ? 'checked': '' }}
                                >

                                <label class="form-check-label" for="hide-unavailable">Hide Unavailable Products</label>
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-md-9">
                @include('common.product-list', ['products' => $products])
            </div>
        </div>

        @if ($products->hasPages())
        	<hr>

            <div class="row justify-content-end">
                <div class="col flex-grow-0">
                    {{ $products->links() }}
                </div>
            </div>
    	@endif
    </div>
@endsection
