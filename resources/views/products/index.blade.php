@extends('layouts.container')

@section('container')
    <div class="row mb-3 align-items-center">
        <div class="col">
            <h3 class="mb-0">Products</h3>
        </div>

        <div class="col flex-grow-0">
            <a href="{{ route('products.create') }}" class="btn btn-primary">
                <span class="fas fa-plus-circle fa-fw"></span> Create a Product
            </a>
        </div>
    </div>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">Administration</li>
        <li class="breadcrumb-item active">Products</li>
    </ol>

    <hr>

    <form method="get" action="{{ url()->current() }}">
        <div class="form-group row">
            <div class="col">
                @include('products.partials.select-order', [
                    'default' => 'alpha'
                ])
            </div>

            <div class="col">
                <input
                    type="text"
                    name="search"
                    placeholder="Search by brand or name"
                    value="{{ request('search') }}"
                    class="form-control"
                >
            </div>

            <div class="col">
                <select-category
                    name="category"
                    :categories="{{ $categories }}"
                    :value="@json(request('category') ? (int) request('category') : null)"
                    placeholder="Filter by category"
                ></select-category>
            </div>

            <div class="col">
                <select-manufacturer
                    name="manufacturer"
                    :manufacturers="{{ $manufacturers }}"
                    :value="@json(request('manufacturer') ? (int) request('manufacturer') : null)"
                    placeholder="Filter by brand"
                ></select-manufacturer>
            </div>

            <div class="col flex-grow-0">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </div>
    </form>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">Price</th>
                <th scope="col">Category</th>
                <th scope="col">Manufacturer</th>
                <th scope="col"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($products as $product)
                <tr>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->getShortDescription() }}</td>
                    <td class="text-nowrap">
                        <product-price :product-id="{{ $product->id }}" :initial-price="{{ $product->price }}"></product-price>
                    </td>
                    <td>
                        <span
                            data-toggle="tooltip"
                            title="{{ $product->category->getHierarchyPath() }}"
                        >{{ $product->category->name }}</span>
                    </td>
                    <td>
                        <a
                            href="{{ route('manufacturers.edit', $product->manufacturer) }}"
                            data-toggle="tooltip"
                            title="Navigate to edit the manufacturer"
                        >{{ $product->manufacturer->name }}</a>
                    </td>
                    <td class="text-right text-nowrap">
                        <a href="#" class="btn btn-outline-primary">
                            <span class="fas fa-truck fa-fw"></span>
                        </a>
                        
                        <a
                            href="{{ route('products.edit', $product) }}"
                            class="d-inline-block btn btn-outline-primary"
                        >
                            <span class="fas fa-edit fa-fw"></span>
                        </a>

                        <link-delete href="{{ route('products.delete', $product) }}"></link-delete>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="row justify-content-center">
        <div class="col flex-grow-0">
            {{ $products->links() }}
        </div>
    </div>
@endsection
