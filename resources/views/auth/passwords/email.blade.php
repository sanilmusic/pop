@extends('layouts.container')

@section('container')
    <h4>Forgot Your Password?</h4>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">Authentication</li>
        <li class="breadcrumb-item active">Forgot Your Password</li>
    </ol>

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <form method="POST" action="{{ route('password.email') }}">
        @csrf

        <div class="form-group row">
            <label for="email" class="col-md-2 col-form-label">Email Address</label>

            <div class="col-md-10">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="text-right">
            <button type="submit" class="btn btn-primary">Send Me Instructions</button>
        </div>
    </form>
@endsection
