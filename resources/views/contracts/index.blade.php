@extends('layouts.container')

@section('container')
    <div class="row mb-3 align-items-center">
        <div class="col">
            <h3 class="mb-0">Contracts</h3>
        </div>

        <div class="col flex-grow-0">
            <a href="{{ route('contracts.create', $supplier) }}" class="btn btn-primary">
                <span class="fas fa-plus-circle fa-fw"></span> Create a Contract
            </a>
        </div>
    </div>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">Administration</li>
        <li class="breadcrumb-item"><a href="{{ route('suppliers.index') }}">Suppliers</a></li>
        <li class="breadcrumb-item">{{ $supplier->name }}</li>
        <li class="breadcrumb-item active">Contracts</li>
    </ol>

    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">Number</th>
                <th scope="col">Responsible Party</th>
                <th scope="col">Date Signed</th>
                <th scope="col"></th>
            </tr>
        </thead>

        <tbody>
            @foreach($contracts as $contract)
                <tr>
                    <td>{{ $contract->contract_number }}</td>
                    <td>{{ $contract->responsible_party }}</td>
                    <td>{{ $contract->created_at->toDateString() }}</td>
                    <td class="text-right">
                        <link-delete href="{{ route('contracts.delete', [$supplier, $contract]) }}"></link-delete>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="row justify-content-center">
        <div class="col flex-grow-0">
            {{ $contracts->links() }}
        </div>
    </div>
@endsection
