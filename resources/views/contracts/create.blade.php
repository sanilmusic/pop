@extends('layouts.container')

@section('container')
    <h3 class="mb-3">Create a Contract</h3>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">Administration</li>
        <li class="breadcrumb-item"><a href="{{ route('suppliers.index') }}">Suppliers</a></li>
        <li class="breadcrumb-item">{{ $supplier->name }}</li>
        <li class="breadcrumb-item"><a href="{{ route('contracts.index', [$supplier]) }}">Contracts</a></li>
        <li class="breadcrumb-item active">Create a Contract</li>
    </ol>

    <form method="POST" action="{{ route('contracts.store', $supplier) }}">
        @csrf

        <div class="form-group row">
            <label for="contract_number" class="col-md-2 col-form-label">Contract Number</label>

            <div class="col-md-10">
                <input id="contract_number" type="text" class="form-control{{ $errors->has('contract_number') ? ' is-invalid' : '' }}" name="contract_number" value="{{ old('contract_number') }}" required autofocus>

                @if ($errors->has('contract_number'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('contract_number') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="responsible_party" class="col-md-2 col-form-label">Responsible Party</label>

            <div class="col-md-10">
                <input id="responsible_party" type="text" class="form-control{{ $errors->has('responsible_party') ? ' is-invalid' : '' }}"  name="responsible_party" value="{{ old('responsible_party') }}" required>

                @if ($errors->has('responsible_party'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('responsible_party') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="text-right">
            <button type="submit" class="btn btn-primary">
                <span class="fas fa-plus-circle fa-fw"></span> Create Contract
            </button>
        </div>
    </form>
@endsection
