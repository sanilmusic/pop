@extends('layouts.app')

@section('content')
    <div class="container h-100">
        <div class="panel-content">
            @yield('container')
        </div>
    </div>
@endsection