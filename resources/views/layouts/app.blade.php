<!DOCTYPE html>
<html class="h-100" lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Shop POP</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app" class="h-100">
        <div class="d-flex flex-column h-100">
            <div class="header-pop container-fluid m-0">
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark navbar-pop">
                    <a class="navbar-brand nav-link-pop" href="{{ url('/') }}">Home</a>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-main" aria-controls="navbar-main" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbar-main">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav">
                            @foreach ($root_categories as $category)
                                <li class="nav-category-link {{ request('category') === $category->id ? ' is-active' : '' }}">
                                    <a
                                        href="{{ route('products.filtered') }}?category={{ $category->id }}"
                                        class="nav-link nav-link-pop"
                                    >{{ $category->name }}</a>
                                </li>
                            @endforeach
                        </ul>

                        <!-- Search -->
                        <form method="get" action="{{ route('products.filtered') }}" class="w-100 ml-4 mr-3">
                            <div class="input-group">
                                <input class="form-control" type="text" name="search" placeholder="Search by product, brand or category" value="{{ request('search') }}">

                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">
                                        <span class="fas fa-search"></span>
                                    </button>
                                </div>
                            </div>
                        </form>

                        <ul class="navbar-nav">
                            @guest
                                <li><a class="nav-link nav-link-pop" href="{{ route('login') }}">Login</a></li>
                                <li><a class="nav-link nav-link-pop" href="{{ route('register') }}">Register</a></li>
                            @endguest

                            @auth
                                <li class="nav-item dropdown dropdown text-nowrap">
                                    <a id="user-dropdown" class="nav-link nav-link-pop dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="fas fa-user fa-fw"></span>
                                        Hi, {{ auth()->user()->name }}
                                    </a>

                                    <div id="user-dropdown" class="dropdown-menu dropdown-menu-right" aria-labelledby="user-dropdown">
                                        <a href="{{ route('account.show') }}" class="dropdown-item">
                                            <span class="fas fa-user-edit fa-fw"></span> My Account
                                        </a>

                                        @can('have', App\Models\Cart::class)
                                            <a class="dropdown-item" href="{{ route('orders.index') }}">
                                                <span class="fas fa-history fa-fw"></span> Previous Orders
                                            </a>
                                        @endcan

                                        @can('manage', App\Models\Category::class)
                                            <a class="dropdown-item" href="{{ route('categories.index') }}">
                                                <span class="fas fa-clipboard-list fa-fw"></span> Categories
                                            </a>
                                        @endcan

                                        @can('manage', App\Models\Manufacturer::class)
                                            <a class="dropdown-item" href="{{ route('manufacturers.index') }}">
                                                <span class="fas fa-industry fa-fw"></span> Manufacturers
                                            </a>
                                        @endcan

                                        @can('manage', App\Models\Product::class)
                                                <a class="dropdown-item" href="{{ route('products.index') }}">
                                                    <span class="fas fa-cube fa-fw"></span> Products
                                                </a>
                                        @endcan

                                        @can('manage', App\Models\Delivery::class)
                                                <a class="dropdown-item" href="{{ route('pending-deliveries.index') }}">
                                                    <span class="fas fa-truck fa-fw"></span> Pending Deliveries
                                                </a>
                                        @endcan

                                        <div class="dropdown-divider"></div>

                                        <a href="#" class="dropdown-item">
                                            <span class="fas fa-envelope fa-fw"></span> Contact Us
                                        </a>

                                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            <span class="fas fa-sign-out-alt fa-fw"></span> Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>

                                @can('have', App\Models\Cart::class)
                                    <li class="nav-item">
                                        <a class="nav-link nav-link-pop" href="{{ route('cart.show') }}">
                                            <span class="fas fa-shopping-cart"></span>
                                        </a>
                                    </li>
                                @endcan

                                <li class="nav-item">
                                    <a href="#" class="nav-link nav-link-pop">
                                        <span class="fas fa-envelope"></span>
                                    </a>
                                </li>
                            @endauth
                        </ul>
                    </div>
                </nav>
            </div>

            @yield('content')

            @section('footer')
                <footer class="page-footer font-small primary pt-4 mt-4">
                    <div class="footer-copyright text-center py-3 footer-bg">
                        © Team Dirty Bits. <a href="https://www.etf.unsa.ba" target="_blank">Faculty of Electrical Engineering</a>.
                    </div>
                </footer>
            @endsection
        </div>

        <portal-target name="modals"></portal-target>
    </div>

    <script src="{{ mix('js/app.js') }}"></script>

    @include('partials.alert')
</body>
</html>
