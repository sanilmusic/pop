@extends('layouts.container')

@section('container')
    <h3 class="mb-3">Pending Deliveries</h3>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">Administration</li>
        <li class="breadcrumb-item active">Pending Deliveries</li>
    </ol>

    <p>
        @if ($deliveries->isEmpty())
            Great job! There are no orders pending a delivery.
        @else
            There are currently <strong>{{ $deliveries->total() }}</strong> orders pending a delivery.
        @endif
    </p>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Pending Since</th>
                <th scope="col">Priority</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($deliveries as $delivery)
                @if ($delivery->getPriority()->equals(\App\Enums\Priority::high()))
                    <tr class="table-danger">
                @elseif ($delivery->getPriority()->equals(\App\Enums\Priority::medium()))
                    <tr class="table-warning">
                @else
                    <tr>
                @endif
                    <td>{{ $delivery->id }}</td>
                    <td>{{ $delivery->cart->finalized_at->diffForHumans() }}</td>
                    <td>{{ ucfirst($delivery->getPriority()) }}</td>
                    <td class="text-right">
                        <button-deliver
                            :delivery-id="{{ $delivery->id }}"
                            :items="{{ $delivery->cart->products }}"
                        ></button-deliver>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    @include('common.centered-pagination', [
        'pagination' => $deliveries
    ])
@endsection
