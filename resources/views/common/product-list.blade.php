<div class="row">
    @foreach($products as $product)
        <div class="col-md-4 mb-3 product-container">
            @can('favorite', App\Models\Product::class)
                <favorite-product
                    class="product-heart"
                    :product-id="{{ $product->id }}"
                    :initial-favorite="@json($product->isFavoritedBy($user))"
                ></favorite-product>
            @endcan

            <div class="position-relative mb-2">
                <image-gallery :initial-images="{{ $product->getImageUrls() }}"></image-gallery>
            </div>

            <h6 class="mb-1">
                @can('manage', $product)
                    <a
                        href="{{ route('products.edit', $product) }}"
                        data-toggle="tooltip"
                        title="Navigate to edit this product"
                    >{{ $product->name }}</a>
                @else
                    {{ $product->name }}
                @endcan
                <br>
                <small class="text-muted">
                    by
                    <a
                        href="{{ route('products.filtered') }}?manufacturer={{ $product->manufacturer->id }}"
                        class="text-muted"
                    >{{ $product->manufacturer->name }}</a>
                </small>
            </h6>

            <div class="row align-items-center">
                <div class="col">
                    <p class="mb-0 text-nowrap">
                        @can('manage', $product)
                            <product-price
                                :product-id="{{ $product->id }}"
                                :initial-price="{{ $product->price }}"
                            ></product-price>
                        @else
                            <span class="product-price">{{ format_currency($product->price) }}</span>
                        @endcan
                    </p>
                </div>

                @can('have', App\Models\Cart::class)
                    <div class="col flex-grow-0">
                        <add-to-cart
                            :product-id="{{ $product->id }}"
                            product-name="{{ $product->name }}"
                            :available-quantity="{{ $product->available_quantity }}"
                        ></add-to-cart>
                    </div>
                @endcan
            </div>
        </div>
    @endforeach
</div>
