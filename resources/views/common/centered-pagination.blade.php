<div class="row justify-content-center">
    <div class="col flex-grow-0">
        {{ $pagination->links() }}
    </div>
</div>
