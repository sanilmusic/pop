@extends('layouts.app')

@section('content')
    <div class="container pt-1 pb-6">
        <h3 class="text-center">My Cart</h3>

        @if ($cart->isEmpty())
            <div class="alert alert-info">Your cart is currently empty. Please use the search field above to look for products by their name, brand or category. You can also take a look at some of the currently most popular products available immediately below.</div>

            <h3 class="mt-4 text-center">Most Popular</h3>

            @include('common.product-list', ['products' => $popular_products])
        @else
            <div class="row">
                @foreach($products as $product)
                    <div class="col-md-6 col-lg-4 mb-3 product-container">
                        @can('favorite', App\Models\Product::class)
                            <favorite-product
                                class="product-heart"
                                :product-id="{{ $product->id }}"
                                :initial-favorite="@json($product->isFavoritedBy($user))"
                            ></favorite-product>
                        @endcan

                        <div class="position-relative mb-2">
                            <image-gallery :initial-images="{{ $product->getImageUrls() }}"></image-gallery>
                        </div>

                        <div class="row mb-1 align-items-baseline">
                            <div class="col">
                                <h6>
                                    {{ $product->name }}<br>
                                    <small class="text-muted">
                                        by
                                        <a
                                            href="{{ route('products.filtered') }}?manufacturer={{ $product->manufacturer->id }}"
                                            class="text-muted"
                                        >{{ $product->manufacturer->name }}</a>
                                    </small>
                                </h6>
                            </div>

                            <div class="col flex-grow-0">
                                <delete-from-cart :product="{{ $product }}"></delete-from-cart>
                            </div>
                        </div>

                        <div class="row align-items-center">
                            <div class="col flex-grow-0">
                                <p class="mb-0 text-nowrap">
                                    <span class="fas fa-fw fa-tag"></span>
                                    <span class="product-price">{{ format_currency($product->price) }}</span>
                                </p>
                            </div>

                            <div class="col">
                                <update-quantity
                                    :product="{{ $product }}"
                                    :purchased-quantity="{{ $product->pivot->quantity }}"
                                    :remaining-quantity="{{ $product->available_quantity }}"
                                ></update-quantity>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="fixed-bottom">
                <div class="cart-control">
                    <div class="row align-items-center">
                        <div class="col">
                            <confirm-link
                                class="btn btn-danger"
                                :class="{ 'disabled': @json($cart->isEmpty()) }"
                                text="Any items you've added to your cart will be deleted."
                                href="/cart/clear"
                            >
                                <span class="fas fa-trash fa-fw"></span>
                                Clear
                            </confirm-link>
                        </div>

                        <div class="col flex-grow-0">
                            <p class="mb-0 text-nowrap">
                                Total: <strong>{{ format_currency($cart->total) }}</strong>
                            </p>
                        </div>

                        <div class="col flex-grow-0">
                            <button-checkout
                                :addresses="{{ $user->addresses }}"
                                :disabled="@json($products->isEmpty())"
                            ></button-checkout>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection

@section('footer')
@overwrite
