@extends('layouts.app')

@section('content')
    <div class="container pt-3">
        <div class="row">
            <div class="col border-right">
                <div class="row border-bottom mb-2">
                    <div class="col">
                        <h4>Account Settings</h4>
                    </div>
                </div>

                <form method="post" action="{{ route('account.update') }}">
                    @csrf
                    <input type="hidden" name="_method" value="patch">

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label">Name</label>
                        <div class="col-md-8">
                            <input id="name" type="text" name="name" value="{{ $user->name }}" class="form-control" disabled>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label">Email Address</label>
                        <div class="col-md-8">
                            <input id="email" type="email" name="email" value="{{ $user->email }}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">

                            @if ($errors->has('name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="new-password" class="col-md-4 col-form-label">New Password</label>
                        <div class="col-md-8">
                            <input id="new-password" type="password" name="new_password" class="form-control{{ $errors->has('new_password') ? ' is-invalid' : '' }}">

                            @if ($errors->has('new_password'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('new_password') }}</strong>
                                </span>
                            @else
                                <small class="form-text text-muted">You can leave this field empty to keep your current password.</small>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="new-password-confirmation" class="col-md-4 col-form-label">Confirm New Password</label>
                        <div class="col-md-8">
                            <input id="new-password-confirmation" type="password" name="new_password_confirmation" class="form-control{{ $errors->has('new_password_confirmation') ? ' is-invalid' : '' }}">

                            @if ($errors->has('new_password_confirmation'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('new_password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col text-right">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col">
                <div class="row mb-2 border-bottom">
                    <div class="col">
                        <h4>My Addresses</h4>
                    </div>

                    <div class="col text-right">
                        <button-address-create></button-address-create>
                    </div>
                </div>

                @if ($user->addresses->isNotEmpty())
                    <account-page-address-list :addresses="{{ $user->addresses }}"></account-page-address-list>
                @else
                    <div class="alert alert-info">
                        You have not saved any addresses to your account yet. Use the button below to add a new address.
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection