/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')

window.Vue = require('vue')

Vue.use(require('portal-vue'))
Vue.use(require('vue-awesome-swiper').default)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const files = require.context('./', true, /\.vue$/i)

files.keys().map(key => {
    const name = _.last(key.split('/')).split('.')[0]

    return Vue.component(name, files(key))
})

const app = new Vue().$mount('#app')

$(document).ready(() => $('[data-toggle=tooltip]').tooltip())
