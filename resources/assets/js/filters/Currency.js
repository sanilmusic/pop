import { formatMoney } from 'accounting-js'

export default function(cents, precision = null) {
    if (precision == null) {
        precision = cents % 100 === 0 ? 0 : 2
    }

    return formatMoney(cents / 100, {
        precision,
        symbol: 'KM',
        decimal: ',',
        thousand: '.',
        format: '%v %s',
    })
}
