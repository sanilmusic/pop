export function when(condition, elements) {
    return condition ? _.castArray(elements) : []
}
