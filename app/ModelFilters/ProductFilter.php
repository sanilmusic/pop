<?php

namespace App\ModelFilters;

use App\Models\Category;
use App\Models\Product;
use EloquentFilter\ModelFilter;

class ProductFilter extends ModelFilter
{
    public function search($search)
    {
        return $this->where(function ($query) use ($search) {
            return $query
                ->whereLike('products.name', '%' . $search . '%')
                ->orWhere('manufacturers.name', $search)
                ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
        });
    }

    public function manufacturer($id)
    {
        return $this->where('manufacturer_id', $id);
    }

    public function category($id)
    {
        $category = Category::find($id);

        return $this->whereIn('category_id', $category ? $category->getDescendants()->pluck('id') : []);
    }

    public function budget($budget)
    {
        return $this->where('price', '<=', $budget);
    }

    public function hideUnavailable()
    {
        return $this->whereRaw(Product::getAvailableQuantitySubquery() . ' > 0');
    }

    public function setup()
    {
        $this
            ->join('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
            ->join('categories', 'categories.id', '=', 'products.category_id');
    }
}
