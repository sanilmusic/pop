<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Cart extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $dates = ['finalized_at', 'deleted_at'];

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('quantity');
    }

    public function shipments()
    {
        return $this->hasMany(CartShipment::class);
    }
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function delivery()
    {
        return $this->hasOne(Delivery::class);
    }

    public function scopeFinalized($query)
    {
        return $query->whereNotNull('finalized_at');
    }

    public function scopeInProgress($query)
    {
        return $query->whereNull('finalized_at');
    }

    public function scopeWithTotal($query)
    {
        return $query->addSubSelect(
            'total',
            DB::table('cart_product')
                ->join('products', 'products.id', '=', 'cart_product.product_id')
                ->whereColumn('cart_product.cart_id', 'carts.id')
                ->selectRaw('SUM(cart_product.quantity * products.price)')
        );
    }

    public function addProduct($product_id, $quantity = 1)
    {
        $product = $this
            ->products
            ->find($product_id);

        if ($product !== null) {
            return $this->updateQuantity($product_id, $product->pivot->quantity + $quantity);
        }

        $this
            ->products()
            ->attach($product_id, compact('quantity'));
    }

    public function deleteProduct($product_id)
    {
        $this
            ->products()
            ->detach($product_id);
    }

    public function updateQuantity($product_id, $quantity)
    {
        $product = $this
            ->products
            ->find($product_id);

        if ($product === null) {
            $this->addProduct($product_id, $quantity);
            return;
        }

        $this
            ->products()
            ->updateExistingPivot($product_id, compact('quantity'));
    }

    public function isEmpty()
    {
        return $this->products->isEmpty();
    }

    public function finalize(Address $shippingAddress)
    {
        $delivery = new Delivery([
            'street' => $shippingAddress->street,
            'postal_code' => $shippingAddress->postal_code,
            'city' => $shippingAddress->city,
        ]);

        $this->delivery()->save($delivery);

        $this->update(['finalized_at' => $this->freshTimestamp()]);
    }
}
