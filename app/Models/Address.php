<?php

namespace App\Models;

use App\Traits\OwnedByModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use OwnedByModel;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopePrimary(Builder $query)
    {
        return $query->where('primary', true);
    }

    public function setAsPrimary()
    {
        return $this->fill(['primary' => true]);
    }

    public function unsetAsPrimary()
    {
        return $this->fill(['primary' => false]);
    }

    public function isPrimary()
    {
        return !!$this->primary;
    }
}
