<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $guarded = [];

    public static function boot()
    {
        parent::boot();

        static::created(function ($model) {
            $model->ratings()->create([
                'price' => 0,
                'shipment_duration' => 0,
                'product_quality' => 0,
                'satisfaction' => 0
            ]);
        });
    }

    public function contracts()
    {
        return $this->hasMany(Contract::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function monthlyRatings()
    {
        return $this->hasMany(Supplier::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function ratings()
    {
        return $this->hasMany(SupplierRating::class);
    }

    public function scopeOrderedAlphabetically($query)
    {
        return $query->orderBy('name');
    }
}
