<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const ADMIN = 1;
    const CUSTOMER = 2;
    const PACKAGER = 3;

    public $timestamps = false;

    protected $guarded = [];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function scopeByName($query, $name)
    {
        return $query->where('name', $name);
    }
}
