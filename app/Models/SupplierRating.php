<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupplierRating extends Model
{
    protected $guarded = [];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function total()
    {
        return $this->price + $this->shipment_speed + $this->product_quality + $this->satisfaction;
    }
}
