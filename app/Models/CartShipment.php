<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartShipment extends Model
{
    protected $guarded = [];

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }
}
