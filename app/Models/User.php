<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    protected $guarded = [];

    protected $with = ['roles', 'roles.permissions'];

    protected $hidden = ['password', 'remember_token'];

    public static function boot()
    {
        parent::boot();

        static::created(function ($user) {
            $user->roles()->attach(Role::CUSTOMER);
            $user->createNewCart();
        });

        static::addGlobalScope(function (Builder $query) {
            return $query->withPrimaryAddress();
        });
    }

    public function carts()
    {
        return $this->hasMany(Cart::class);
    }

    public function currentCart()
    {
        return $this->hasOne(Cart::class)->latest();
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function favorites()
    {
        return $this->belongsToMany(Product::class)->withTimestamps();
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function primaryAddress()
    {
        return $this->belongsTo(Address::class);
    }

    public function scopeWithPrimaryAddress(Builder $query)
    {
        return $query->addSubSelect(
            'primary_address_id',
            Address::select('id')->whereColumn('user_id', 'users.id')->primary()->getQuery()
        );
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    public function hasPermission($permission)
    {
        return $this
            ->roles
            ->pluck('permissions')
            ->flatten()
            ->unique()
            ->pluck('name')
            ->contains($permission);
    }

    public function createNewCart()
    {
        $this
            ->carts()
            ->create();
    }

    public function clearCurrentCart()
    {
        if ($this->currentCart && !$this->currentCart->isEmpty()) {
            $this->currentCart->delete();
            $this->createNewCart();
        }
    }

    public function finalizeCurrentCart(Address $shippingAddress, $saveShippingAddress = false)
    {
        if ($saveShippingAddress) {
            $this->saveAddress($shippingAddress);
        }

        $this->currentCart->finalize($shippingAddress);
        $this->createNewCart();
    }

    public function updatePrimaryAddress(Address $address)
    {
        if ($this->hasPrimaryAddress()) {
            $this->clearPrimaryAddress();
        }

        $address->setAsPrimary()->save();
    }

    public function clearPrimaryAddress()
    {
        if ($this->hasPrimaryAddress()) {
            $this->primaryAddress->unsetAsPrimary()->save();
        }
    }

    public function saveAddress(Address $address)
    {
        if (!$this->hasPrimaryAddress()) {
            $address->setAsPrimary();
        }

        if ($address->isPrimary()) {
            $this->clearPrimaryAddress();
        }

        return $this->addresses()->save($address);
    }

    public function hasPrimaryAddress()
    {
        return !!$this->primaryAddress;
    }

    public function deleteAddress(Address $address)
    {
        $address->delete();

        if ($address->isPrimary() && $newPrimary = $this->addresses()->first()) {
            $newPrimary->setAsPrimary()->save();
        }
    }
}
