<?php

namespace App\Models;

use App\Traits\OwnedByModel;
use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    use OwnedByModel;

    protected $guarded = [];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function scopeOrderedByDateSigned($query)
    {
        return $query->orderBy('created_at', 'desc');
    }
}
