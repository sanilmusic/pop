<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Enums\Priority;

class Delivery extends Model
{
	public $timestamps = false;
	protected $guarded = [];
    protected $dates = ['shipped_at'];

    public function cart()
    {
    	return $this->belongsTo(Cart::class);
    }

    public function scopePending($query)
    {
    	return $query->whereNull('shipped_at');
    }

    public function scopeOrderByPriority($query)
    {
        return $query
            ->join('carts', 'carts.id', '=', 'cart_id')
            ->oldest('carts.finalized_at');
    }

	public function getPriority()
    {
    	$daysOld = $this->cart->finalized_at->diffInDays();

    	if ($daysOld >= 2) {
    		return Priority::high();
    	} elseif ($daysOld >= 1) {
    		return Priority::medium();
    	}

    	return Priority::low();
	}
	
	public function markAsShipped()
	{
		$this->update(['shipped_at' => $this->freshTimestamp()]);
	}

    public function isShipped()
    {
        return !!$this->shipped_at;
    }
}
