<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use EloquentFilter\Filterable;
use App\Traits\OwnedByModel;

class Product extends Model implements HasMedia
{
    use OwnedByModel, HasMediaTrait, Filterable;

    protected $guarded = [];

    protected $casts = [
        'available_quantity' => 'integer'
    ];

    protected $perPage = 6;

    public static function getAvailableQuantitySubquery()
    {
        $totalReceivedQuery = Item::selectRaw('sum(quantity)')
            ->whereColumn('product_id', 'products.id');

        $totalSoldQuery = Cart::selectRaw('sum(cart_product.quantity)')
            ->join('cart_product', 'carts.id', '=', 'cart_product.cart_id')
            ->whereColumn('cart_product.product_id', 'products.id');

        return sprintf(
            'ifnull((%s), 0) - ifnull((%s), 0)',
            $totalReceivedQuery->toSql(),
            $totalSoldQuery->toSql()
        );
    }

    public function carts()
    {
        return $this->belongsToMany(Cart::class)->withPivot('quantity');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class);
    }

    public function monthlyRecommendations()
    {
        return $this->hasMany(MonthlyRecommendation::class);
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }

    public function suppliers()
    {
        return $this->belongsToMany(Supplier::class);
    }

    public function favoritedBy()
    {
        return $this->belongsToMany(User::class, 'favorites')->withTimestamps();
    }

    public function scopeApplyOrder($query, $order, $default_order = 'price_lh')
    {
        if ($order === 'favorites_first' || $order === 'favorites_last') {
            $query->withFavoriteFlag();
        } elseif ($order === 'popularity') {
            $query->withPopularity();
        }

        [$column, $direction] = $this->getArgumentsForOrder($order ?? $default_order);

        return $query->orderBy($column, $direction);
    }

    public function scopeWithAvailableQuantity($query)
    {
        return $query
            ->select('products.*')
            ->selectRaw(static::getAvailableQuantitySubquery() . ' as available_quantity');
    }

    public function scopeWithFavoriteFlag($query, $user = null)
    {
        $user = $user ?? user();

        return $query->addSubSelect(
            'favorite',
            DB::table('favorites')
                ->whereColumn('product_id', 'products.id')
                ->where('user_id', $user->id)
                ->selectRaw('COUNT(*) > 0')
        );
    }

    public function scopeWithPopularity($query, $sinceHours = 48)
    {
        return $query->addSubSelect(
            'popularity',
            DB::table('cart_product')
                ->join('carts', 'cart_product.id', '=', 'carts.id')
                ->whereColumn('cart_product.product_id', 'products.id')
                ->whereRaw('TIMESTAMPDIFF(HOUR, carts.finalized_at, NOW()) < ?', $sinceHours)
                ->selectRaw('SUM(cart_product.quantity)')
        );
    }

    protected function getArgumentsForOrder($order)
    {
        switch ($order) {
            case 'latest':
                return ['created_at', 'desc'];

            case 'price_hl':
                return ['price', 'desc'];

            case 'favorites_first':
            case 'favorites_last':
                return ['favorite', $order === 'favorites_last' ? 'asc' : 'desc'];

            case 'popularity':
                return ['popularity', 'desc'];

            case 'alpha':
                return ['name', 'asc'];

            default:
                return ['price', 'asc'];
        }
    }

    public function getShortDescription()
    {
        return Str::limit($this->description, 50);
    }

    public function getImageUrls()
    {
        return $this
            ->getMedia('images')
            ->map->getUrl();
    }

    public function isFavoritedBy(User $user)
    {
        return $this->favoritedBy->contains($user);
    }

    public function getPriceInDollars()
    {
        return $this->price / 100;
    }

    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = $value * 100;
    }
}
