<?php

namespace App\Models;

use Illuminate\Support\Collection;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_category_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_category_id');
    }

    public function scopeByNames($query, $names)
    {
        return $query->whereIn('name', Arr::wrap($names));
    }

    public function scopeRoot($query)
    {
        return $query->whereNull('parent_category_id');
    }

    public function scopeWithAncestors($query, $depth = 5)
    {
        $with = [];

        for ($i = 1; $i <= $depth; $i++) {
            $with[] = implode('.', array_fill(0, $i, 'parent'));
        }

        return $query->with($with);
    }

    public function scopeWithDescendants($query, $depth = 5)
    {
        $with = [];

        for ($i = 1; $i <= $depth; $i++) {
            $with[] = implode('.', array_fill(0, $i, 'children'));
        }

        return $query->with($with);
    }

    public function isRoot()
    {
        return !$this->parent;
    }

    public function getAncestors()
    {
        $ancestors = Collection::wrap($this);

        if (!$this->isRoot()) {
            $ancestors = $ancestors->concat($this->parent->getAncestors());
        }

        return $ancestors;
    }

    public function getDescendants()
    {
        return Collection::wrap($this)
            ->concat($this->children->flatMap->getDescendants());
    }

    public function getHierarchy()
    {
        return $this->getAncestors()->reverse();
    }

    public function getHierarchyPath()
    {
        return $this
            ->getHierarchy()
            ->map->name
            ->implode(' > ');
    }
}
