<?php

namespace App\Support;

class Alert
{
    public const DEFAULT_TIMER = 3000;

    protected $attributes = [];

    public function __call($name, $arguments)
    {
        $value = $arguments[0];

        return $value ? $this->set($name, $value) : $this->unset($name);
    }

    public function timer($timer = null)
    {
        return $this->set('timer', $timer ?? static::DEFAULT_TIMER);
    }

    public function success($text)
    {
        return $this
            ->titleText('Well done!')
            ->text($text)
            ->type('success')
            ->timer();
    }

    protected function set($attribute, $value)
    {
        $this->attributes[$attribute] = $value;

        return $this->persist();
    }

    protected function unset($attribute)
    {
        unset($this->attributes[$attribute]);

        return $this->persist();
    }

    protected function persist()
    {
        session()->put('alert', $this->attributes);

        return $this;
    }
}