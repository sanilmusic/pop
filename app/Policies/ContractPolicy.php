<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ContractPolicy
{
    use HandlesAuthorization;

    public function manage(User $user)
    {
        return $user->hasPermission('manage-contracts');
    }
}
