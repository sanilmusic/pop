<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    public function manage(User $user)
    {
        return $user->hasPermission('manage-products');
    }

    public function favorite(User $user)
    {
    	return $user->hasPermission('have-favorites');
    }
}
