<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SupplierPolicy
{
    use HandlesAuthorization;

    public function manage(User $user)
    {
        return $user->hasPermission('manage-suppliers');
    }
}
