<?php

namespace App\Policies;

use App\Models\Address;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AddressPolicy
{
    use HandlesAuthorization;

    public function delete(User $user, Address $address)
    {
        return $address->isOwnedBy($user);
    }
}
