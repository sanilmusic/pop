<?php

namespace App\Rules;

use App\Models\Product;
use Illuminate\Contracts\Validation\Rule;

class ProductAvailable implements Rule
{
    protected $quantity;
    protected $offset;

    public function __construct($quantity = 1, $offset = 0)
    {
        $this->quantity = $quantity;
        $this->offset = $offset;
    }

    public function passes($attribute, $value)
    {
        $product = Product::query()
            ->withAvailableQuantity()
            ->find($value);

        return ($this->quantity - $this->offset) <= $product->available_quantity;
    }

    public function message()
    {
        return 'The selected product is not currently available in the desired quantity.';
    }
}
