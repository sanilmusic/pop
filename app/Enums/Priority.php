<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * @method static self high()
 * @method static self medium()
 * @method static self low()
 */
class Priority extends Enum
{
}
