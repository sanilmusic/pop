<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    public function index()
    {
        return view('articles.index', [
            'items' => Product::withAvailableQuantity()
                ->get()
        ]);
    }

    public function showFilteredByCategory(Request $request)
    {
        return view('articles.index', [
            'items' => Product::withAvailableQuantity()
                ->where('category_id', $request->category_id)
                ->get()
        ]);
    }
}
