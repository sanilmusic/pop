<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Manufacturer;
use App\Models\Category;
use App\Http\Requests\Products\UpdateProductRequest;
use App\Http\Requests\Products\UpdateProductPriceRequest;
use App\Http\Requests\Products\CreateProductRequest;

class ProductsController extends Controller
{
    public function create()
    {
        return view('products.create', [
            'categories' => Category::all(),
            'manufacturers' => Manufacturer::all(),
        ]);
    }

    public function store(CreateProductRequest $request)
    {
        $attributes = Arr::except($request->validated(), 'images');

    	$product = Product::create($attributes);

    	foreach ($request->images as $image) {
    	    $product->addMedia($image)->toMediaCollection('images');
        }

    	return redirect()->route('products.index')->with('success', 'Product successfully created.');
    }

    public function index(Request $request)
    {
        $products = Product::query()
            ->withAvailableQuantity()
            ->filter($request->only('search', 'manufacturer', 'category'))
            ->applyOrder($request->order, 'alphabetically')
            ->paginate(10);

        return view('products.index', [
            'categories' => Category::all(),
            'manufacturers' => Manufacturer::all(),
            'products' => $products,
        ]);
    }

    public function edit(Product $product)
    {
        return view('products.edit', [
            'product' => $product,
            'categories' => Category::all(),
            'manufacturers' => Manufacturer::all(),
        ]);
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        $product->update(array_filter($request->validated()));

        return redirect()->route('products.edit', $product)->with(
            'success',
            "The changes you've made to the product have been saved."
        );
    }

    public function updatePrice(UpdateProductPriceRequest $request, Product $product)
    {
        $product->update($request->validated());
    }

    public function delete(Product $product)
    {
        $product->delete();

        return redirect()->route('products.index')->with('success', 'Product successfully deleted.');
    }

    public function filtered(Request $request)
    {
        $products = Product::with('manufacturer', 'media', 'favoritedBy')
            ->withAvailableQuantity()
            ->filter($request->only('search', 'manufacturer', 'category', 'budget', 'hide_unavailable'))
            ->applyOrder($request->order);

        $aggregate = Product::with('manufacturer', 'media', 'favoritedBy')
            ->filter($request->only('search', 'manufacturer', 'category'));

        return view('products.filtered', [
            'user' => $request->user(),
            'categories' => Category::all(),
            'manufacturers' => Manufacturer::all(),
            'products' => $products->paginateFilter(),
            'min_price' => $aggregate->min('price'),
            'max_price' => $aggregate->max('price'),
        ]);
    }
}
