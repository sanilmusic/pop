<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Requests\Categories\UpdateCategoryRequest;
use App\Http\Requests\Categories\CreateCategoryRequest;

class CategoriesController extends Controller
{
    public function create()
    {
    	return view('categories.create', [
    	    'categories' => Category::all()
        ]);
    }

    public function store(CreateCategoryRequest $request)
    {
        Category::create($request->validated());

    	return redirect()->route('categories.index')->with('success', 'Category successfully created.');
    }

    public function index()
    {
        $categories = Category::withAncestors()
            ->withDescendants()
            ->paginate(10);

        return view('categories.index', compact('categories'));
    }

    public function edit(Category $category)
    {
        return view('categories.edit', [
            'category' => $category,
            'categories' => Category::all(),
        ]);
    }

    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $category->update($request->validated());

        return redirect()->route('categories.edit', $category)->with(
            'success',
            "The changes you've made to the category have been saved."
        );
    }

    public function delete(Category $category)
    {
        $category->delete();

        return redirect()->route('categories.index')->with('success', 'Category successfully deleted.');
    }
}
