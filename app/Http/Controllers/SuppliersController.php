<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSupplierRequest;
use App\Models\Supplier;

class SuppliersController extends Controller
{
    public function create()
    {
    	return view('suppliers.create');
    }

    public function store(CreateSupplierRequest $request)
    {
    	Supplier::create($request->validated());

    	return redirect()->route('suppliers.index');
    }

    public function index()
    {
        return view('suppliers.index', [
            'suppliers' => Supplier::orderedAlphabetically()->paginate(10)
        ]);
    }

    public function delete(Supplier $supplier)
    {
        $supplier->delete();

        return redirect()->route('suppliers.index');
    }

}
