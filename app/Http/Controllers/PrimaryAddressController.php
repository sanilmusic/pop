<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdatePrimaryAddressRequest;
use App\Models\Address;

class PrimaryAddressController extends Controller
{
    public function update(UpdatePrimaryAddressRequest $request)
    {
        $request->user()->updatePrimaryAddress(
            Address::find($request->address_id)
        );
    }
}
