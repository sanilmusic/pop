<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Product;
use App\Models\Address;
use App\Http\Requests\UpdateQuantityRequest;
use App\Http\Requests\DeleteProductFromCartRequest;
use App\Http\Requests\CheckoutRequest;
use App\Http\Requests\AddProductToCartRequest;

class CartController extends Controller
{
    public function show(Request $request)
    {
        $user = $request->user();
        $cart = $user->currentCart()->withTotal()->first();

        $products = $cart
            ->products()
            ->with('manufacturer', 'media')
            ->withAvailableQuantity()
            ->get();

        $popular_products = Product::with('manufacturer', 'media')
            ->withAvailableQuantity()
            ->applyOrder('popularity')
            ->take(3)
            ->get();

    	return view('cart.show', compact('user', 'cart', 'products', 'popular_products'));
    }

    public function addProduct(AddProductToCartRequest $request)
    {
        $request
            ->user()
            ->currentCart
            ->addProduct($request->product_id);
    }

    public function deleteProduct(DeleteProductFromCartRequest $request)
    {
        $request
            ->user()
            ->currentCart
            ->deleteProduct($request->product_id);
    }

    public function updateQuantity(UpdateQuantityRequest $request)
    {
        $request
            ->user()
            ->currentCart
            ->updateQuantity($request->product_id, $request->quantity);
    }

    public function clear(Request $request)
    {
        $request->user()->clearCurrentCart();

        return redirect()->route('cart.show')->with('success', 'A category has been created.');
    }

    public function checkout(CheckoutRequest $request)
    {
        $shippingAddress = new Address(
            Arr::except($request->validated(), 'save_address')
        );

        $request->user()->finalizeCurrentCart($shippingAddress, $saveShippingAddress = $request->save_address);
    }
}
