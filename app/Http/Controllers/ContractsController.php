<?php

namespace App\Http\Controllers;

use App\Models\Contract;
use App\Http\Requests\CreateContractRequest;
use App\Models\Supplier;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ContractsController extends Controller
{
    public function create(Supplier $supplier)
    {
    	return view('contracts.create', compact('supplier'));
    }

    public function store(CreateContractRequest $request, Supplier $supplier)
    {
        $supplier->contracts()->create($request->validated());

    	return redirect()->route('contracts.index', $supplier);
    }

    public function index(Supplier $supplier)
    {
        $contracts = $supplier->contracts()->orderedByDateSigned()->paginate(10);

        return view('contracts.index', compact('supplier', 'contracts'));
    }

    public function delete(Supplier $supplier, Contract $contract)
    {
        if (!$contract->isOwnedBy($supplier)) {
            return new NotFoundHttpException();
        }

        $contract->delete();

        return redirect()->route('contracts.index', $supplier);
    }
}
