<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Address;
use App\Http\Requests\CreateAddressRequest;

class AddressesController extends Controller
{
    public function store(CreateAddressRequest $request)
    {
        $request->user()->saveAddress(new Address($request->validated()));
    }

    public function destroy(Address $address, Request $request)
    {
        $this->authorize($address);

        $request->user()->deleteAddress($address);
    }
}
