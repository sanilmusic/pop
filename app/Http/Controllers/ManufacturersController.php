<?php

namespace App\Http\Controllers;

use App\Models\Manufacturer;
use App\Http\Requests\Manufacturers\UpdateManufacturerRequest;
use App\Http\Requests\Manufacturers\CreateManufacturerRequest;

class ManufacturersController extends Controller
{
    public function create()
    {
    	return view('manufacturers.create');
    }

    public function store(CreateManufacturerRequest $request)
    {
    	Manufacturer::create($request->validated());

    	return redirect()->route('manufacturers.index')->with('success', 'Manufacturer successfully created.');
    }

    public function index()
    {
        return view('manufacturers.index', [
            'manufacturers' => Manufacturer::orderedAlphabetically()->paginate(10)
        ]);
    }

    public function edit(Manufacturer $manufacturer)
    {
        return view('manufacturers.edit', compact('manufacturer'));
    }

    public function update(UpdateManufacturerRequest $request, Manufacturer $manufacturer)
    {
        $manufacturer->update($request->validated());

        return redirect()->route('manufacturers.edit', $manufacturer)->with(
            'success',
            "The changes you've made to the manufacturer have been saved."
        );
    }

    public function delete(Manufacturer $manufacturer)
    {
        $manufacturer->delete();

        return redirect()->route('manufacturers.index')->with('success', 'Manufacturer successfully created.');
    }
}
