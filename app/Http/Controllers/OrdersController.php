<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function index(Request $request)
    {
    	$user = $request->user();

    	$popular_products = Product::with('manufacturer', 'media')
            ->withAvailableQuantity()
            ->applyOrder('popularity')
            ->take(3)
            ->get();

        $orders = $user
       		->carts()
       		->with('products', 'delivery')
       		->withTotal()
       		->finalized()
       		->get();

    	return view('orders.index', compact('user', 'popular_products', 'orders'));
    }
}
