<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateAccountRequest;

class AccountController extends Controller
{
    public function show(Request $request)
    {
    	return view('account.show', [
    		'user' => $request->user()
    	]);
    }

    public function update(UpdateAccountRequest $request)
    {
        $user = $request->user();

        if ($request->new_password) {
            $user->password = $request->new_password;
        }

    	$request->user()->update(
    	    Arr::except($request->validated(), 'new_password')
        );

    	return redirect()->route('account.show')->with('success', 'Your account settings have been updated.');
    }
}
