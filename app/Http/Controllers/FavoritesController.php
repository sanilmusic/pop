<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class FavoritesController extends Controller
{
    public function store(Request $request, Product $product)
    {
        $product->favoritedBy()->syncWithoutDetaching($request->user());

        return [
            'favorite' => true
        ];
    }

    public function destroy(Request $request, Product $product)
    {
        $product->favoritedBy()->detach($request->user());

        return [
            'favorite' => false
        ];
    }
}
