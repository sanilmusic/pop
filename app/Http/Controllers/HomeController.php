<?php

namespace App\Http\Controllers;

use App\Models\Product;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unavailableItems = Product::withAvailableQuantity()
            ->where('available_quantity', 0)
            ->get();

        $soonToBeUnavailable = Product::withAvailableQuantity()
            ->whereBetween('available_quantity', [0, 5])
            ->get();

        return view('home', compact('unavailableItems', 'soonToBeUnavailable'));
    }
}
