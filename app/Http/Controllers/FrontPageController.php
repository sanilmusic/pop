<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class FrontPageController extends Controller
{
    public function index(Request $request)
    {
        $popular_products = Product::with('manufacturer', 'media')
            ->withAvailableQuantity()
            ->applyOrder('popularity')
            ->take(3)
            ->get();

        return view('frontPage.index', [
            'user' => $request->user(),
            'popular_products' => $popular_products
        ]);
    }
}
