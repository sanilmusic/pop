<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSupplierRatingRequest;
use App\Models\Supplier;
use App\Models\SupplierRating;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class SuppliersRatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ratings = [];
        $suppliers = Supplier::all();

        foreach (SupplierRating::latest()->get() as $rate) {
            $index = Arr::where($ratings, function ($rating) use ($rate) {
                return $rating->supplier_id === $rate->supplier_id;
            });

            if (!$index) {
                $ratings[] = $rate;
            }
        }

        usort($ratings, function ($rating1, $rating2) {
            $totalRate1 = $rating1->total();
            $totalRate2 = $rating2->total();

            $diff = $totalRate1 - $totalRate2;

            return ($diff < 0) - ($diff > 0);
        });


        return view('suppliers.rate', ['suppliers' => Supplier::all(), 'ratings' => $ratings]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CreateSupplierRatingRequest $request)
    {
        SupplierRating::create(['supplier_id' => $request->supplier_id, 'price' => $request->price, 'shipment_speed' => $request->shipment_speed, 'product_quality' => $request->product_quality, 'satisfaction' => $request->satisfaction]);
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateSupplierRatingRequest $request)
    {
        SupplierRating::where('supplier_id', $request->supplier_id)->update($request->validated());
        $sortedSuppliers = Supplier::join('suppliers_rates AS sr', 'sr.supplier_id', '=', 'suppliers.id')
            ->orderBy(DB::raw('sr.price + sr.shipment_speed + sr.product_quality + sr.satisfaction'), 'desc')
            ->select('suppliers.*')
            ->get();

        return view('suppliers.rate', ['suppliers' => Supplier::all(), 'sortedSuppliers' => $sortedSuppliers]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ratingHistory(Request $request){

        $showsuppliers = Supplier::all();
        if($request->has('supplier_id')){
            $showsuppliers = Supplier::where('id', $request->supplier_id)->get();
        }

        return view('suppliers.rate-history', ['suppliers' => Supplier::all(), 'showsuppliers' => $showsuppliers]);

    }

    public function ratingHistoryChart(Request $request){
        $arrayOfGrades = [];
        $selectedSupplier = Supplier::where('id', $request->supplier_id)->first();
        $sortedSuppliers = Supplier::join('suppliers_rates AS sr', 'sr.supplier_id', '=', 'suppliers.id')
            ->orderBy(DB::raw('sr.price + sr.shipment_speed + sr.product_quality + sr.satisfaction'), 'desc')
            ->select('suppliers.*');
        if($request->has('supplier_id')){
            $sortedSuppliers = $sortedSuppliers->where('supplier_id', $request->supplier_id);
        }

        $arrayOfGrades = SupplierRating::where('supplier_id', $request->supplier_id)->select(DB::raw('price + shipment_speed + product_quality + satisfaction as totalGrade, created_at as date'))->get();

        return view('suppliers.rate-history-chart', ['suppliers' => Supplier::all(), 'sortedSuppliers' => $sortedSuppliers->get(), 'arrayOfGrades' => $arrayOfGrades, 'selectedSupplier' => $selectedSupplier]);

    }

}
