<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Delivery;

class PendingDeliveriesController extends Controller
{
    public function index()
    {
    	return view('pending-deliveries.index', [
    		'deliveries' => Delivery::with('cart')->pending()->orderByPriority()->paginate()
    	]);
    }

    public function ship(Delivery $delivery)
    {
        $delivery->markAsShipped();
    }
}
