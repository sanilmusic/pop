<?php

namespace App\Http\Requests\Products;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required|numeric|min:0',
            'images' => 'required|array',
            'images.*' => 'required|file|mimetypes:image/jpeg,image/png,image/webp',
            'category_id' => 'required|integer|exists:categories,id',
            'manufacturer_id' => 'required|integer|exists:manufacturers,id',
        ];
    }
}
