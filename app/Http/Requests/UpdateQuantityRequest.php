<?php

namespace App\Http\Requests;

use App\Rules\ProductAvailable;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateQuantityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => [
                'required',
                'integer',
                'exists:products,id',
                new ProductAvailable($this->quantity, $this->getCurrentQuantity())
            ],
            'quantity' => [
                'required',
                'integer',
                'min:1'
            ]
        ];
    }

    protected function getCurrentQuantity()
    {
        $product = $this
            ->user()
            ->currentCart
            ->products()
            ->find($this->product_id);

        return $product === null ? 0 : $product->pivot->quantity;
    }
}
