<?php

function format_currency($cents, $suffix = 'KM', $decimal = ',', $thousand = '.')
{
	return number_format($cents / 100, 2, $decimal, $thousand) . ' ' . $suffix;
}

function user()
{
    return auth()->user();
}

function alert()
{
    return app(\App\Support\Alert::class);
}