<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

trait OwnedByModel
{
    public function scopeOwnedBy(Builder $query, Model $model, $foreignKey = null)
    {
        return $query->where($this->getOwnerForeignKey($model, $foreignKey), $model->getKey());
    }

    public function isOwnedBy(Model $model, $foreignKey = null)
    {
        return $this->getAttribute($this->getOwnerForeignKey($model, $foreignKey)) === $model->getKey();
    }

    protected function getOwnerForeignKey(Model $model, $foreignKey = null)
    {
        return $foreignKey ?? $model->getForeignKey();
    }
}