<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('supplier_id')->unsigned();;
            $table->integer('price');
            $table->integer('shipment_duration');
            $table->integer('product_quality');
            $table->integer('satisfaction');
            $table->timestamps();

            $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_ratings');
    }
}
