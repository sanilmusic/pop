<?php

use App\Models\Role;
use Illuminate\Database\Migrations\Migration;

class AddCustomerRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Role::create([
            'name' => 'customer'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Role::byName('customer')->delete();
    }
}
