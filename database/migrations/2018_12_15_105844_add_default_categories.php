<?php

use App\Models\Category;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Str;

class AddDefaultCategories extends Migration
{
    protected const DEFAULT_CATEGORIES = ['Women', 'Men'];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (static::DEFAULT_CATEGORIES as $name) {
            Category::create([
                'name' => $name,
                'slug' => Str::slug($name)
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Category::byNames(static::DEFAULT_CATEGORIES)->delete();
    }
}
